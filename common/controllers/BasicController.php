<?php

namespace common\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;

/**
 * BasicController
 */
class BasicController extends Controller
{
	protected $acciones; 
	public $userCan;
	public function beforeAction($action)
	{
    	$this->loadActions();
		$this->loadUserCan();
	    if (!parent::beforeAction($action)) {
	        return false;
	    }
	 	
	    $operacion = str_replace("/", "-", Yii::$app->controller->route);
		
		if(!\Yii::$app->user->can($operacion)){
			throw new ForbiddenHttpException('No estás autorizado');
		}
	 
	    return true;
	}
	
	protected function loadActions(){
		$this->acciones=array_filter(get_class_methods($this),[$this, 'accion']);
		foreach($this->acciones as $k=>$accion){
			$this->acciones[$k]=substr(strtolower(preg_replace('/([A-Z])/', '-$1', str_replace('action', '', $accion))),1);
		}
	}
	
	protected function loadUserCan(){
		
		$permisoPre='';
		if(isset(\Yii::$app->controller->module)){
			$module=\Yii::$app->controller->module->id;
			$permisoPre=$module.'-';
		}
		$controller=\Yii::$app->controller->id;
		$permisoPre.=$controller;
		foreach($this->acciones as $accion){
			$this->userCan[$permisoPre.'-'.$accion]=\Yii::$app->user->can($permisoPre.'-'.$accion);
		}
	}
	
	protected function accion($var){
		return (strpos($var, 'action')!==false)&&($var!='actions');
	}
}
?>