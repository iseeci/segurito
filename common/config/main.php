<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@app/modules/user/views'
                ],
            ],
        ],
    ],
	'modules' => [
		'user' => [
			'class' => 'dektrium\user\Module',
	        'controllerMap' => [
                'admin' => 'frontend\modules\user\controllers\AdminController',
                'profile' => 'frontend\modules\user\controllers\ProfileController',
            ],
		],
		'rbac' => [
			'class' => 'dektrium\rbac\Module',
		],
	],
];
