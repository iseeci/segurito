<?php

namespace frontend\modules\base\models;

use Yii;
use dektrium\user\models\User;
use frontend\components\securitybehaviors\StripTagsBehavior;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "f290".
 *
 * @property integer $id
 * @property integer $pais_id
 * @property integer $aseguradora_id
 * @property integer $ramo_id
 * @property integer $renglon_id
 * @property integer $mes_id
 * @property double $valor
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $status
 *
 * @property Aseguradora $aseguradora
 * @property User $createdBy
 * @property Pais $pais
 * @property Ramo $ramo
 * @property RenglonF290 $renglon
 * @property User $updatedBy
 */
class F290 extends \yii\db\ActiveRecord
{
    public $codigo_super_aseguradora;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'f290';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pais_id', 'aseguradora_id', 'ramo_id', 'renglon_id', 'mes_id', 'valor'], 'required'],
            [['pais_id', 'aseguradora_id', 'ramo_id', 'renglon_id', 'mes_id', 'created_by', 'updated_by'], 'integer'],
            [['valor'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['status'], 'string', 'max' => 255]
        ];
    }
	
	public function behaviors(){
		return [
			'stripTags' => ['class' => StripTagsBehavior::className(),], 
			'timestamp' => [
	            'class' => TimestampBehavior::className(),
	            'attributes' => [
	                ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
	                ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
	            ],
	            'value' => new Expression('NOW()'),
             ],
			'blameable' => ['class' => BlameableBehavior::className(),],
        ];
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pais_id' => Yii::t('app', 'Pais ID'),
            'aseguradora_id' => Yii::t('app', 'Aseguradora ID'),
            'ramo_id' => Yii::t('app', 'Ramo ID'),
            'renglon_id' => Yii::t('app', 'Renglon ID'),
            'mes_id' => Yii::t('app', 'Mes ID'),
            'valor' => Yii::t('app', 'Valor'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAseguradora()
    {
        return $this->hasOne(Aseguradora::className(), ['id' => 'aseguradora_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPais()
    {
        return $this->hasOne(Pais::className(), ['id' => 'pais_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRamo()
    {
        return $this->hasOne(Ramo::className(), ['id' => 'ramo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRenglon()
    {
        return $this->hasOne(RenglonF290::className(), ['id' => 'renglon_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
