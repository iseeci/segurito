<?php

namespace frontend\modules\base\models;

use Yii;
use frontend\components\securitybehaviors\StripTagsBehavior;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "aseguradora".
 *
 * @property integer $id
 * @property string $codigo_super
 * @property string $nombre
 * @property string $descripcion
 * @property integer $pais_id
 * @property integer $region_id
 * @property integer $ciudad_id
 * @property string $fecha_fundacion
 * @property integer $numero_ramos
 * @property integer $numero_empleados
 * @property integer $numero_paises_operacion
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $status
 *
 * @property Ciudad $ciudad
 * @property User $createdBy
 * @property Pais $pais
 * @property Region $region
 * @property User $updatedBy
 * @property AseguradoraRamoPais[] $aseguradoraRamoPais
 * @property F290[] $F290s
 */
class Aseguradora extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'aseguradora';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codigo_super', 'nombre'], 'required'],
            [['pais_id', 'region_id', 'ciudad_id', 'numero_ramos', 'numero_empleados', 'numero_paises_operacion', 'created_by', 'updated_by'], 'integer'],
            [['fecha_fundacion', 'created_at', 'updated_at'], 'safe'],
            [['codigo_super', 'nombre', 'status'], 'string', 'max' => 255],
            [['descripcion'], 'string', 'max' => 1200]
        ];
    }
	
	public function behaviors(){
		return [
			'stripTags' => ['class' => StripTagsBehavior::className(),], 
			'timestamp' => [
	            'class' => TimestampBehavior::className(),
	            'attributes' => [
	                ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
	                ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
	            ],
	            'value' => new Expression('NOW()'),
             ],
			'blameable' => ['class' => BlameableBehavior::className(),],
        ];
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'codigo_super' => Yii::t('app', 'Codigo Super'),
            'nombre' => Yii::t('app', 'Nombre'),
            'descripcion' => Yii::t('app', 'Descripcion'),
            'pais_id' => Yii::t('app', 'Pais ID'),
            'region_id' => Yii::t('app', 'Region ID'),
            'ciudad_id' => Yii::t('app', 'Ciudad ID'),
            'fecha_fundacion' => Yii::t('app', 'Fecha Fundacion'),
            'numero_ramos' => Yii::t('app', 'Numero Ramos'),
            'numero_empleados' => Yii::t('app', 'Numero Empleados'),
            'numero_paises_operacion' => Yii::t('app', 'Numero Paises Operacion'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCiudad()
    {
        return $this->hasOne(Ciudad::className(), ['id' => 'ciudad_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPais()
    {
        return $this->hasOne(Pais::className(), ['id' => 'pais_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAseguradoraRamoPais()
    {
        return $this->hasMany(AseguradoraRamoPais::className(), ['aseguradora_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getF290s()
    {
        return $this->hasMany(F290::className(), ['aseguradora_id' => 'id']);
    }
}
