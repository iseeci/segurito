<?php

namespace frontend\modules\base;

class Base extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\base\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
