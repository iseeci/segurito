<?php
use scotthuangzl\googlechart\GoogleChart;

/* @var $this yii\web\View */

echo $this->render('_filtroForm',[
        	'model'=>$model,
        	'aseguradoras'=>$aseguradoras,
        	'ramos'=>$ramos,
        	'paises'=>$paises,
        	'renglones'=>$renglones,
        	'meses_ids'=>$meses_ids,
        ]);
		$title=isset($model->renglon)?$model->renglon->nombre:'';
		$title.=isset($model->ramo)?' - '.$model->ramo->nombre:'';
		 echo GoogleChart::widget(['visualization' => 'PieChart',
                'data' => $data['pie'],
                'options' => ['title' => $title,'height'=>600]]);
?>


