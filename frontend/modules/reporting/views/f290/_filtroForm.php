<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\multiselect\MultiSelect;

/* @var $this yii\web\View */
/* @var $model frontend\modules\reporting\models\FiltroRenglonForm */
/* @var $form ActiveForm */
?>
<div class="filtroForm">

    <?php $form = ActiveForm::begin(); ?>
		<?= $form->errorSummary($model); ?>
	    <div class="row-fluid">
	        <div class="span4">
	        	<?= $form->field($model, 'pais_id')->dropDownList($paises,['prompt'=>Yii::t('app','Selecciona País')]) ?>
	        </div>
        </div>
        <div class="form-group">
        <div class="row-fluid">
        <?= MultiSelect::widget([
    		'options' => ['multiple'=>'multiple'], // for the actual multiselect
		    'data' => $aseguradoras,
		    'model' => $model,
		    'attribute' => 'aseguradora_id',
		    'clientOptions' => 
		        [
		            'includeSelectAllOption' => true,
		        ], 
		]) ?>
		</div>
        <div class="row-fluid">
        <?= MultiSelect::widget([
    		'options' => ['multiple'=>'multiple'], // for the actual multiselect
		    'data' => $ramos,
		    'model' => $model,
		    'attribute' => 'ramo_id',
		    'clientOptions' => 
		        [
		            'includeSelectAllOption' => true,
		        ], 
		]) ?>
		</div>
        <div class="row-fluid">
        <?= MultiSelect::widget([
    		'options' => ['multiple'=>'multiple'], // for the actual multiselect
		    'data' => $renglones,
		    'model' => $model,
		    'attribute' => 'renglon_id',
		    'clientOptions' => 
		        [
		            'includeSelectAllOption' => true,
		        ], 
		]) ?>
		</div>
		</div>
        <?= $form->field($model, 'mes_inicial')->dropDownList($meses_ids,['prompt'=>Yii::t('app','Selecciona mes inicial')]) ?>
        <?= $form->field($model, 'mes_final')->dropDownList($meses_ids,['prompt'=>Yii::t('app','Selecciona mes final')]) ?>
    
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- _filtroForm -->
