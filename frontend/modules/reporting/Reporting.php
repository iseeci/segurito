<?php

namespace frontend\modules\reporting;

class Reporting extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\reporting\controllers';

    public function init()
    {
        parent::init();
    }
}
