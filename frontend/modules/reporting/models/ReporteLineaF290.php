<?php

namespace frontend\modules\reporting\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use frontend\modules\base\models\F290;
use frontend\modules\base\models\Aseguradora;
use frontend\modules\base\models\Ramo;
use frontend\modules\base\models\Pais;
use frontend\modules\base\models\RenglonF290;

/**
 * ContactForm is the model behind the contact form.
 */
class ReporteLineaF290 extends Model
{
    public $aseguradora_id;
    public $ramo_id;
    public $pais_id;
    public $renglon_id;
    public $mes_inicial;
    public $mes_final;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['aseguradora_id', 'ramo_id', 'pais_id', 'renglon_id', 'mes_inicial', 'mes_final'], 'required'],
            [['pais_id'],'integer'],
            ['aseguradora_id', 'each', 'rule'=>['integer']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pais_id' => Yii::t('app', 'Pais ID'),
            'aseguradora_id' => Yii::t('app', 'Aseguradora ID'),
            'ramo_id' => Yii::t('app', 'Ramo ID'),
            'renglon_id' => Yii::t('app', 'Renglon ID'),
            'mes_id' => Yii::t('app', 'Mes ID'),
        ];
    }
	
	public function searchSimple($format='googlecharts'){
		$data=F290::find()->select('mes_id, a.codigo_super as codigo_super_aseguradora, sum(valor) as valor')
			->innerJoin('aseguradora a',' a.id=f290.aseguradora_id')
			->where(['f290.pais_id'=>$this->pais_id])
			->andWhere(['in','aseguradora_id',$this->aseguradora_id])
			->andWhere(['in','ramo_id',$this->ramo_id])
			->andWhere(['in','renglon_id',$this->renglon_id])
			->andWhere(['between','mes_id', $this->mes_inicial, $this->mes_final])->groupBy('mes_id,a.codigo_super')->orderBy('mes_id,a.codigo_super')->all();
		$aseguradoras=ArrayHelper::map($data,'codigo_super_aseguradora','codigo_super_aseguradora');
		asort($aseguradoras);
		switch($format){
			case 'googlecharts':
				$columns=['Mes'];
				//Genere los meses ids incluidos en el filtro
				//Todo: incluir el filtro de aseguradoras
				$formattedData=[array_merge(['Mes'],array_values($aseguradoras))];
				$k=0;
				for ($i=$this->mes_inicial; $i < $this->mes_final; ($i%100!=12)?$i++:$i=round($i/100)*100+101) {
					$formattedDataRow=[];
					$formattedDataRow[]=$data[$k]->mes_id;
					$j=0;
					foreach($aseguradoras as $id=>$codigo_super){
						if($data[$k]->mes_id==$i && $data[$k]->codigo_super_aseguradora==$codigo_super){
							$formattedDataRow[]=floatval($data[$k]->valor)/1000000;
							$k++;
						}else{
							$formattedDataRow[]=0.0;
						}
						$j++;
					}
					$formattedData[]=$formattedDataRow;
				}
				break;
			default:
				$formattedData=$data;
		}
		return $formattedData;
	}

	public function getRenglon(){
		return RenglonF290::find()->where(['id'=>$this->renglon_id])->one();
	}

	public function getRamo(){
		return Ramo::find()->where(['id'=>$this->ramo_id])->one();
	}
}
