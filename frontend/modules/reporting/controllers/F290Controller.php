<?php
namespace frontend\modules\reporting\controllers;

use Yii;
use frontend\modules\base\models\Aseguradora;
use frontend\modules\base\models\Ramo;
use frontend\modules\geo\models\Pais;
use frontend\modules\base\models\RenglonF290;
use frontend\modules\base\models\F290;
use yii\helpers\ArrayHelper;
use frontend\modules\reporting\models\ReportePieF290;
use frontend\modules\reporting\models\ReporteLineaF290;

class F290Controller extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
	
    public function actionPieRenglonF290()
    {
    	$model=new ReportePieF290;
		$pieData=[];
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
        	$pieData=$model->searchSimple();
			//var_dump($pieData);die;
        }
		
		$aseguradoras=Aseguradora::find()->where(['status'=>'activa'])->all();
		$aseguradoras=ArrayHelper::map($aseguradoras,'id','nombre');
		$ramos=Ramo::find()->where(['status'=>'activo'])->all();
		$ramos=ArrayHelper::map($ramos,'id','nombre');
		$paises=Pais::find()->where(['status'=>'activo'])->all();
		$paises=ArrayHelper::map($paises,'id','nombre');
		$renglones=RenglonF290::find()->where(['status'=>'activo'])->all();
		$renglones=ArrayHelper::map($renglones,'id','nombre');
		$meses_ids=F290::find()->select('mes_id as id, mes_id')->where(['status'=>'activo'])->groupBy(['mes_id'])->all();
		$meses_ids=ArrayHelper::map($meses_ids,'id','mes_id');
		
        return $this->render('pie',[
        	'model'=>$model,
        	'aseguradoras'=>$aseguradoras,
        	'ramos'=>$ramos,
        	'paises'=>$paises,
        	'renglones'=>$renglones,
        	'meses_ids'=>$meses_ids,
        	'data'=>['pie'=>$pieData],
        ]);
    }
	
    public function actionLineaRenglonF290()
    {
    	$model=new ReporteLineaF290;
		$lineData=[];
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
        	$lineData=$model->searchSimple();
			//var_dump($pieData);die;
        }
		
		$aseguradoras=Aseguradora::find()->where(['status'=>'activa'])->all();$aseguradoras=ArrayHelper::map($aseguradoras,'id','nombre');
		$ramos=Ramo::find()->where(['status'=>'activo'])->all();$ramos=ArrayHelper::map($ramos,'id','nombre');
		$paises=Pais::find()->where(['status'=>'activo'])->all();$paises=ArrayHelper::map($paises,'id','nombre');
		$renglones=RenglonF290::find()->where(['status'=>'activo'])->all();$renglones=ArrayHelper::map($renglones,'id','nombre');
		$meses_ids=F290::find()->select('mes_id as id, mes_id')->where(['status'=>'activo'])->groupBy(['mes_id'])->all();
		$meses_ids=ArrayHelper::map($meses_ids,'id','mes_id');
		
        return $this->render('linea',[
        	'model'=>$model,
        	'aseguradoras'=>$aseguradoras,
        	'ramos'=>$ramos,
        	'paises'=>$paises,
        	'renglones'=>$renglones,
        	'meses_ids'=>$meses_ids,
        	'data'=>['line'=>$lineData],
        ]);
    }
}
