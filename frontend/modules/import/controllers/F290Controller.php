<?php

namespace frontend\modules\import\controllers;

use yii\web\Controller;

class F290Controller extends Controller
{
	public function actionCsv()
    {
        return $this->render('csv');
    }
}
