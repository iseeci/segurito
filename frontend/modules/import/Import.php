<?php

namespace frontend\modules\import;

class Import extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\import\controllers';

    public function init()
    {
        parent::init();
    }
}
