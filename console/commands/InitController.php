<?php
namespace console\commands;

use Yii;
use yii\console\Controller;
use yii\helpers\Console;
use yii\db\Connection;
use frontend\modules\user\models\ConsoleUser;
use dektrium\user\Finder;
use dektrium\user\models\User;
use dektrium\user\models\Token;
use frontend\models\DireccionParametro;
use frontend\modules\base\models\Ramo;

class InitController extends Controller
{
	public $admin_id=1, $ingeniero1_id=3, $ingeniero2_id=4, $ingeniero3_id=5, $manager1_id=6, $manager2_id=7;
	public $colombia_id=1, $antioquia_id=1, $medellin_id=1, $cundinamarca_id=2, $bogota_id=2, $valle_id=3, $cali_id=3;
	
	public function actionFull(){
		$this->actionUsersRbac();
		$this->actionAppPars();
		$this->actionGeo();;
		$this->actionRamos();
	}
	
	public function actionFullDummy(){
		$this->actionFull();
	}
	
	public function actionUsersRbac(){
    	$cn=\Yii::$app->db;
		
		echo 'Inicializando tablas de usuarios'.PHP_EOL;
		$cn->createCommand('set foreign_key_checks=0;
			truncate token;truncate social_account;foreign_key_checks=1;')->execute();
		$cn->createCommand('set foreign_key_checks=0;truncate social_account;foreign_key_checks=1;')->execute();
		$cn->createCommand('set foreign_key_checks=0;truncate profile;foreign_key_checks=1;')->execute();
		$cn->createCommand('set foreign_key_checks=0;truncate user;foreign_key_checks=1;')->execute();
		
		$fernandrez=$this->createUser('fernandrez@gmail.com','fernandrez','fernandrez2015');
		
		echo 'Inicializando tablas de autenticacion'.PHP_EOL;
		$cn->createCommand('set foreign_key_checks=0;truncate auth_item_child;
			truncate auth_assignment;
			truncate auth_item;
			truncate auth_rule;
			foreign_key_checks=1;')->execute();
        $auth = Yii::$app->authManager;
		
		echo 'Creando permisos'.PHP_EOL;
        //Controladores y sus acciones
		$actions['base']=['index','view'];
        $actions['geo']['pais']=['update','create','delete'];
        $actions['geo']['region']=['update','create','delete','get-regiones-pais'];
        $actions['geo']['ciudad']=['update','create','delete','get-ciudades-region'];
        $actions['corte']['lamina']=['create','update','delete','create-batch','row'];
        
        $modules=array_filter(array_keys($actions),function($v){return $v!='base';});
		
		foreach($modules as $m){
			$controllers=array_keys($actions[$m]);
			foreach($controllers as $c){
				$actions[$m][$c]=array_merge($actions['base'],isset($actions[$m][$c])?$actions[$m][$c]:[]);
			}
		}
		
        $fcp='full-controller-permission';
		foreach($modules as $m){
			$controllers=array_keys($actions[$m]);
			foreach($controllers as $c){
				$permissions[$m][$c][$fcp]=$auth->createPermission($m.'-'.$c.'-'.$fcp);
				$permissions[$m][$c][$fcp]->description = $m.'-'.$c.'-'.$fcp;
				$auth->add($permissions[$m][$c][$fcp]);
				foreach($actions[$m][$c] as $a){
					$permissions[$m][$c][$a]=$auth->createPermission($m.'-'.$c.'-'.$a);
					$permissions[$m][$c][$a]->description = $m.'-'.$c.'-'.$a;
					$auth->add($permissions[$m][$c][$a]);
					$auth->addChild($permissions[$m][$c][$fcp],$permissions[$m][$c][$a]);
				}
			}		
		}		
		
		echo 'Creando roles'.PHP_EOL;
        $adminRole = $auth->createRole('admin');
        $auth->add($adminRole);
		
		echo 'Asignando permisos a rol admin'.PHP_EOL;
        foreach($permissions as $m=>$permission){
        	foreach($permission as $c=>$p){
        		$auth->addChild($adminRole, $p[$fcp]);
			}
		}
		
		echo 'Asignando roles a usuarios'.PHP_EOL;
		//Admin
        $auth->assign($adminRole, $fernandrez->id);$this->admin_id=$fernandrez->id;
    }

	public function actionAppPars(){
    	$cn=\Yii::$app->db;
		
		echo 'Inicializando tabla de parametros de aplicacion'.PHP_EOL;
		$cn->createCommand('set foreign_key_checks=0;truncate parametro;foreign_key_checks=1;')->execute();
		$fields=[];
		$data=[];
		//$this->batchInsert('frontend\models\Parametro',$fields,$data);
	}

	public function actionGeo(){
    	$cn=\Yii::$app->db;
		
		echo 'Inicializando tablas de geografia'.PHP_EOL;
		$cn->createCommand('set foreign_key_checks=0;truncate ciudad;truncate region;truncate pais;foreign_key_checks=1;')->execute();
		$fields=['pais_cd', 'nombre', 'created_by'];
		$data=[
			['CO','Colombia',$this->admin_id],
		];
		
		echo 'Insertando paises'.PHP_EOL;
		$ids_paises=$this->batchInsert('frontend\modules\geo\models\Pais',$fields,$data);
		
		$this->colombia_id=$ids_paises[0];
		
		$fields=['pais_id', 'region_cd', 'nombre', 'created_by'];
		$data=[
			[$this->colombia_id, 'ANT','Antioquia',$this->admin_id],
			[$this->colombia_id, 'CUN','Cundinamarca',$this->admin_id],
			[$this->colombia_id, 'VAL','Valle',$this->admin_id],
		];
		
		echo 'Insertando regiones'.PHP_EOL;
		$ids_regiones=$this->batchInsert('frontend\modules\geo\models\Region',$fields,$data);
		
		
		$this->antioquia_id=$ids_regiones[0];
		$this->cundinamarca_id=$ids_regiones[1];
		$this->valle_id=$ids_regiones[2];
		
		$fields=['pais_id', 'region_id', 'nombre', 'created_by'];
		$data=[
			[$this->colombia_id, $this->antioquia_id,'Medellín',$this->admin_id],
			[$this->colombia_id, $this->antioquia_id,'Envigado',$this->admin_id],
			[$this->colombia_id, $this->antioquia_id,'Itagüí',$this->admin_id],
			[$this->colombia_id, $this->antioquia_id,'Sabaneta',$this->admin_id],
			[$this->colombia_id, $this->antioquia_id,'Bello',$this->admin_id],
			[$this->colombia_id, $this->cundinamarca_id,'Bogotá',$this->admin_id],
			[$this->colombia_id, $this->cundinamarca_id,'Chía',$this->admin_id],
			[$this->colombia_id, $this->valle_id,'Cali',$this->admin_id],
		];
		
		echo 'Insertando ciudades'.PHP_EOL;
		$ids_ciudades=$this->batchInsert('frontend\modules\geo\models\Ciudad',$fields,$data);
		
		$this->medellin_id=$ids_ciudades[0];
		$this->bogota_id=$ids_ciudades[1];
		$this->cali_id=$ids_ciudades[2];
	}

	private function createUser($email,$username,$password){
		
		$user = Yii::createObject([
            'class'    => User::className(),
            'scenario' => 'create',
            'email'    => $email,
            'username' => $username,
            'password' => $password,
        ]);

        if ($user->create()) {
            $this->stdout(Yii::t('user', 'User has been created') . "!\n", Console::FG_GREEN);
        } else {
            $this->stdout(Yii::t('user', 'Please fix following errors:') . "\n", Console::FG_RED);
            foreach ($user->errors as $errors) {
                foreach ($errors as $error) {
                    $this->stdout(' - ' . $error . "\n", Console::FG_RED);
                }
            }
        }
		return $user;
	}
	
	public function actionRamos(){
		$cn=\Yii::$app->db;
		
		echo 'Inicializando tabla de ramos'.PHP_EOL;
		$cn->createCommand('set foreign_key_checks=0;truncate ramo;foreign_key_checks=1;')->execute();
		$fields=['nombre','tipo','codigo_super'];
		$data=[
			['TOTAL','total','001'],
			['SUBTOTAL - RAMOS','subtotal','002'],
			['AUTOMOVILES','generales','003'],
			['SOAT','generales','004'],
			['CUMPLIMIENTO','generales','005'],
			['RESPONSABILIDAD CIVIL','generales','006'],
			['INCENDIO','generales','007'],
			['TERREMOTO','generales','008'],
			['SUSTRACCION','generales','009'],
			['TRANSPORTE','generales','010'],
			['CORRIENTE DÉBIL','generales','011'],
			['TODO RIESGO CONTRATISTA','generales','012'],
			['MANEJO','generales','013'],
			['LUCRO CESANTE','generales','014'],
			['MONTAJE Y ROTURA DE MAQUINARIA','generales','015'],
			['AVIACIÓN','generales','016'],
			['NAVEGACIÓN Y CASCO','generales','017'],
			['MINAS Y PETRÓLEOS','generales','018'],
			['VIDRIOS','generales','019'],
			['CRÉDITO COMERCIAL','generales','020'],
			['CRÉDITO A LA EXPORTACIÓN','generales','021'],
			['AGRÍCOLA','generales','022'],
			['SEMOVIENTES','generales','023'],
			['DESEMPLEO','generales','024'],
			['HOGAR','generales','025'],
			['EXEQUIAS','vida','026'],
			['ACCIDENTES PERSONALES','vida','027'],
			['COLECTIVO VIDA','vida','028'],
			['EDUCATIVO','vida','029'],
			['VIDA GRUPO','vida','030'],
			['SALUD','vida','031'],
			['ENFERMEDADES DE ALTO COSTO','vida','032'],
			['VIDA INDIVIDUAL','vida','033'],
			['PREVISIONAL DE INVALIDEZ.Y SOBREVIVENCIA','vida','034'],
			['RIESGOS LABORALES','vida','035'],
			['PENSIONES LEY 100','vida','036'],
			['PENSIONES VOLUNTARIAS','vida','037'],
			['PENSIONES CON CONMUTACIÓN PENSIONAL','vida','038'],
			['PATRIM. AUTÓNOMO - FDO. PENS. VOLUNTARIAS','vida','039'],
			['RENTAS VOLUNTARIAS','vida','040'],
			['- BEPS','vida','041'],
		];
		
		echo 'Insertando ramos'.PHP_EOL;
		$ids_paises=$this->batchInsert('frontend\modules\base\models\Ramo',$fields,$data);
	}

	private function batchInsert($model,$fields,$data){
		$ret=[];
		foreach($data as $d){
			$m=new $model;$setA=[];	
			foreach($fields as $k=>$f){
				$setA[$f]=$d[$k];
			}
			$m->setAttributes($setA);
			$m->detachBehavior('blameable');
			$m->save();
			if(count($m->errors)>0){
				var_dump($m->errors);
				var_dump($m->attributes);die;
			}
			$ret[]=$m->id;
		}
		return $ret; 
	}
}