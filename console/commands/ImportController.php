<?php
namespace console\commands;

use Yii;
use yii\console\Controller;
use yii\helpers\Console;
use yii\helpers\FileHelper;
use yii\db\Connection;
use frontend\modules\user\models\ConsoleUser;
use dektrium\user\Finder;
use dektrium\user\models\User;
use dektrium\user\models\Token;
use frontend\models\DireccionParametro;
use console\models\ImportF290;

class ImportController extends Controller
{
	public function actionF290(){
		$import=new ImportF290;
		$import->scan();
	}
	
	private function batchInsert($model,$fields,$data){
		$ret=[];
		foreach($data as $d){
			$m=new $model;$setA=[];	
			foreach($fields as $k=>$f){
				$setA[$f]=$d[$k];
			}
			$m->setAttributes($setA);
			$m->detachBehavior('blameable');
			$m->save();
			if(count($m->errors)>0){
				var_dump($m->errors);
				var_dump($m->attributes);die;
			}
			$ret[]=$m->id;
		}
		return $ret; 
	}
}