<?php
namespace console\models;

use Yii;
use yii\base\Model;
use yii\helpers\FileHelper;
use yii\helpers\ArrayHelper;
use frontend\modules\import\models\ImportHistory;
use frontend\modules\base\models\Aseguradora;
use frontend\modules\base\models\F290;
use frontend\modules\base\models\RenglonF290;
use frontend\modules\geo\models\Pais;

class ImportF290 extends Model{
	public $fields=['aseguradora_id','ramo_id','pais_id','renglon_id','mes_id','signo','valor','valor_signo','created_at'];
	public function scan($secure=false){
		$pais_id=Pais::find()->where(['lower(nombre)'=>'colombia'])->one()->id;
		$filesFull=FileHelper::findFiles(Yii::getAlias('@app').'/../etls/import/f290',['only'=>['*.csv']]);
		$cn=\Yii::$app->db;
		foreach($filesFull as $fileFull){
			$filename=substr($fileFull, strrpos($fileFull, '\\')+1);
			echo $filename.PHP_EOL;
			$mes=substr($filename, 4, 2); $ano=substr(date('Y'), 0, 2).substr($filename, 6, 2);
			$mes_id=$ano.$mes;
			$importado=ImportHistory::find()->where(['filename'=>$filename])->one();
			if(!$importado || $importado->status!='importado'){
				if(!$importado){
					$importado=new ImportHistory;
				}
				$importado->setAttributes([
					'filename'=>$filename,
					'full_path'=>$fileFull,
				]);
				if(!$importado->save()){
					var_dump($importado->errors);die;
				}
				
				$handle = fopen($fileFull,"r");
				$sep=';';
			    $matrix = file_get_contents($fileFull);
				$matrix=mb_convert_encoding($matrix, "UTF-8", "UTF-8");
				$lines = explode(PHP_EOL, $matrix);
				
				//Compañías y renglones
				$lineCount=count($lines);
				$cont=0;
				$sqlHeader='INSERT INTO f290 ('.implode(',',$this->fields).') VALUES ';
				$sqlValues='';
				foreach($lines as $k=>$line)
				{
					//Si está en el primer renglon revise las compañías
					if($k==0){
				 		$headers = explode($sep, $line);
						//echo '<pre>';var_dump($headers);echo '</pre>';die;
						$count=count($headers);
						//Mirar si hay compañías que no existían
						for ($i=2; $i < count($headers); $i++) { 
							$aseguradora=Aseguradora::find()->where(['codigo_super'=>trim($headers[$i])])->one();
							//Si no existía la agrega
							if(!$aseguradora){
								$aseguradora=new Aseguradora;
								$aseguradora->setAttributes([
									'codigo_super'=>trim($headers[$i]),
									'nombre'=>trim($headers[$i]),
								]);
								//Si no salva por algun motivo muestre los errores y pare
								if(!$aseguradora->save()){
									echo 'Aseguradora '.$aseguradora->codigo_super.PHP_EOL;
									var_dump($aseguradora->errors);
									die;
								}
							}
							$aseCodigoId[$aseguradora->codigo_super]=$aseguradora->id;
						}
					} else { //Si no esta en el primer renglón es un renglón normal
						//Mirar si el renglón ya existe (último cinco dígitos)
				 		$data = explode($sep, $line);
						if(count($data)==$count && strtolower($data[0])!='(forcolucaren)'){
							$ramo_id=intval(substr($data[0], 4, 3));
							$codigoRenglon=substr($data[0], 5);
							$codigoRenglon=str_pad(($codigoRenglon % 100000),5,'0',STR_PAD_LEFT);
							$renglon=RenglonF290::find()->where(['codigo_super'=>$codigoRenglon])->one();
							if(!$renglon){
								$renglon=new RenglonF290;
								$renglon->setAttributes([
									'codigo_super'=>trim($codigoRenglon),
									'nombre'=>trim($data[1]),
									'signo'=>(strpos($data[1], '(resta)')!==false)?1:0,
								]);
								//Si no salva por algun motivo muestre los errores y pare
								if(!$renglon->save()){
									echo 'Renglon '.$codigoRenglon.PHP_EOL;
									var_dump($renglon->errors);
									//die;
								}
							}
							//Pida todo el renglon de los meses anteriores
							if($mes>1){
								//var_dump([intval($ano.'01'),($mes_id-1)]);
								$aseramparen1=F290::find()
										->select('aseguradora_id,sum(valor) as valor')
										->where([
											'ramo_id'=>$ramo_id,
											'pais_id'=>$pais_id,
											'renglon_id'=>$renglon->id,
										])
										->andWhere(['between','mes_id',intval($ano.'01'),($mes_id-1)])
										->groupBy('aseguradora_id')
										->all();
								$aseramparen1=ArrayHelper::map($aseramparen1,'aseguradora_id','valor');
							}
							//Guarde los valores en la compañía
							for ($i=2; $i < count($data); $i++) {
								if($data[$i]!=0){
									if($secure){
										$aseramparen=F290::find()
											->where([
												'aseguradora_id'=>$aseCodigoId[trim($headers[$i])],
												'ramo_id'=>$ramo_id,
												'pais_id'=>$pais_id,
												'mes_id'=>$mes_id,
												'renglon_id'=>$renglon->id,
											])->one();
									}
									if(!$secure || !$aseramparen){
										$valor=floatval($data[$i]);
										//echo 'Dato '.trim($headers[$i]).' '.$renglon->nombre.': '. $valor.PHP_EOL;
										if(isset($aseramparen1[$aseCodigoId[trim($headers[$i])]])){
											//echo 'Suma :'.$aseramparen1[$aseCodigoId[trim($headers[$i])]].PHP_EOL;
											$valor-=$aseramparen1[$aseCodigoId[trim($headers[$i])]];
										}
										//echo 'Neto: '.$valor.PHP_EOL;
										$aseramparen=new F290;
										$aseramparen->setAttributes([
											'aseguradora_id'=>$aseCodigoId[trim($headers[$i])],
											'ramo_id'=>$ramo_id,
											'pais_id'=>$pais_id,
											'mes_id'=>$mes_id,
											'renglon_id'=>$renglon->id,
											'valor'=>$valor,
										]);
										//Si no salva por algun motivo muestre los errores y pare
										if(!$aseramparen->validate()){
											var_dump($aseramparen->errors);
											die;
										}else{
											$sqlValues.='('.
												$aseramparen->aseguradora_id.','.
												$aseramparen->ramo_id.','.
												$aseramparen->pais_id.','.
												$aseramparen->renglon_id.','.
												$aseramparen->mes_id.','.
												$renglon->signo.','.
												$aseramparen->valor.','.
												pow(-1,$renglon->signo)*$aseramparen->valor.','.
												'"'.date('Y-m-d H:i:s').'"'.
											'),';
											$cont++;
											if($cont % 10000 == 0){
												$sqlValues=substr($sqlValues, 0, strlen($sqlValues)-1).';';
												$cn->createCommand($sqlHeader.$sqlValues)->execute();
												$sqlValues='';
											}
										}
									}
								}
							}
						}
					}
					if($k%100==0){
						echo (($k/$lineCount)*100).'%'.PHP_EOL;
					} 
				}
				if(strlen($sqlValues)>1){
					$sqlValues=substr($sqlValues, 0, strlen($sqlValues)-1).';';
					$cn->createCommand($sqlHeader.$sqlValues)->execute();
				}
				$importado->status='importado';$importado->save();
			}	
		}
	}
}
