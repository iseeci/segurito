<?php

use yii\db\Schema;
use yii\db\Migration;

class m150821_224900_seguro extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%aseguradora}}', [
            'id' => $this->primaryKey(),
            'codigo_super' => $this->string()->notNull(),
            'nombre' => $this->string()->notNull(),
            'descripcion' => $this->string(1200),
            'pais_id' => $this->integer(),
            'region_id' => $this->integer(),
            'ciudad_id' => $this->integer(),
            'fecha_fundacion' => $this->datetime(),
            'numero_ramos' => $this->integer(),
            'numero_empleados' => $this->integer(),
            'numero_paises_operacion' => $this->integer(),
            'created_at' => $this->datetime()->notNull(),
			'updated_at' => $this->datetime(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			'status' => $this->string()->notNull()->defaultValue('activa')
        ], $tableOptions);
		
		$this->addForeignKey('fk_aseguradora_ciudad','{{%aseguradora}}','ciudad_id','{{%ciudad}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_aseguradora_region','{{%aseguradora}}','region_id','{{%region}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_aseguradora_pais','{{%aseguradora}}','pais_id','{{%pais}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_aseguradora_created_by','{{%aseguradora}}','created_by','{{%user}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_aseguradora_updated_by','{{%aseguradora}}','updated_by','{{%user}}','id','CASCADE','RESTRICT');
		
		$this->createTable('{{%ramo}}', [
            'id' => $this->primaryKey(),
            'codigo_super' => $this->string()->notNull(),
            'nombre' => $this->string()->notNull(),
            'descripcion' => $this->string(1200),
            'tipo' => $this->string()->notNull(),
            'created_at' => $this->datetime()->notNull(),
			'updated_at' => $this->datetime(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			'status' => $this->string()->notNull()->defaultValue('activo')
        ], $tableOptions);
		
		$this->addForeignKey('fk_ramo_created_by','{{%ramo}}','created_by','{{%user}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_ramo_updated_by','{{%ramo}}','updated_by','{{%user}}','id','CASCADE','RESTRICT');
		
		$this->createTable('{{%aseguradora_ramo_pais}}', [
            'id' => $this->primaryKey(),
            'aseguradora_id' => $this->integer()->notNull(),
            'ramo_id' => $this->integer()->notNull(),
            'pais_id' => $this->integer()->notNull(),
            'created_at' => $this->datetime()->notNull(),
			'updated_at' => $this->datetime(),
			'created_by' => $this->integer()->notNull(),
			'updated_by' => $this->integer(),
			'status' => $this->string()->notNull()->defaultValue('activo')
        ], $tableOptions);
		
		$this->addForeignKey('fk_aseguradora_ramo_pais_aseguradora','{{%aseguradora_ramo_pais}}','aseguradora_id','{{%aseguradora}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_aseguradora_ramo_pais_ramo','{{%aseguradora_ramo_pais}}','ramo_id','{{%ramo}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_aseguradora_ramo_pais_pais','{{%aseguradora_ramo_pais}}','pais_id','{{%pais}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_aseguradora_ramo_pais_created_by','{{%aseguradora_ramo_pais}}','created_by','{{%user}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_aseguradora_ramo_pais_updated_by','{{%aseguradora_ramo_pais}}','updated_by','{{%user}}','id','CASCADE','RESTRICT');
		
		$this->createTable('{{%renglon_f290}}', [
            'id' => $this->primaryKey(),
            'codigo_super' => $this->string()->notNull(),
            'nombre' => $this->string()->notNull(),
            'descripcion' => $this->string(),
            'nombres_alternativos' => $this->string(),
            'signo' => $this->boolean()->notNull(),
            'created_at' => $this->datetime()->notNull(),
			'updated_at' => $this->datetime(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			'status' => $this->string()->notNull()->defaultValue('activo')
        ], $tableOptions);
		
		$this->addForeignKey('fk_renglon_created_by','{{%renglon_f290}}','created_by','{{%user}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_renglon_updated_by','{{%renglon_f290}}','updated_by','{{%user}}','id','CASCADE','RESTRICT');
		
		$this->createTable('{{%f290}}', [
            'id' => $this->primaryKey(),
            'aseguradora_id' => $this->integer()->notNull(),
            'ramo_id' => $this->integer()->notNull(),
            'pais_id' => $this->integer()->notNull(),
            'renglon_id' => $this->integer()->notNull(),
            'mes_id' => $this->integer()->notNull(),
            'signo' => $this->boolean()->notNull(),
            'valor' => $this->double()->notNull(),
            'valor_signo' => $this->double()->notNull(),
            'created_at' => $this->datetime()->notNull(),
			'updated_at' => $this->datetime(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			'status' => $this->string()->notNull()->defaultValue('activo')
        ], $tableOptions);
		
		$this->addForeignKey('fk_f290_pais','{{%f290}}','pais_id','{{%pais}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_f290_aseguradora','{{%f290}}','aseguradora_id','{{%aseguradora}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_f290_ramo','{{%f290}}','ramo_id','{{%ramo}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_f290_renglon','{{%f290}}','renglon_id','{{%renglon_f290}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_f290_created_by','{{%f290}}','created_by','{{%user}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_f290_updated_by','{{%f290}}','updated_by','{{%user}}','id','CASCADE','RESTRICT');
		
		$this->createTable('{{%import_history}}', [
            'id' => $this->primaryKey(),
            'filename' => $this->string()->notNull()->unique(),
            'full_path' => $this->string()->notNull()->unique(),
            'created_at' => $this->datetime()->notNull(),
			'updated_at' => $this->datetime(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			'status' => $this->string()->notNull()->defaultValue('detectado')
        ], $tableOptions);
		
    	$this->addForeignKey('fk_import_created_by','{{%import_history}}','created_by','{{%user}}','id','CASCADE','RESTRICT');
		$this->addForeignKey('fk_import_updated_by','{{%import_history}}','updated_by','{{%user}}','id','CASCADE','RESTRICT');
	}

    public function down()
    {
        $this->dropTable('{{%import_history}}');
        $this->dropTable('{{%f290}}');
        $this->dropTable('{{%renglon_f290}}');
        $this->dropTable('{{%aseguradora_ramo_pais}}');
        $this->dropTable('{{%ramo}}');
        $this->dropTable('{{%aseguradora}}');
    }
}
